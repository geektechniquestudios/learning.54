package com.geektechnique.actuatorrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuatorRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActuatorRestApplication.class, args);
    }

}
